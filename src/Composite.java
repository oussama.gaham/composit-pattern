import java.util.ArrayList;
import java.util.List;

public class Composite extends Component {
  private final List<Component> components;

  public Composite(String name) {
    super(name);
    this.components = new ArrayList<>();
  }

  public void add(Component c) {
    c.level = this.level + 1;
    components.add(c);
  }

  public void remove(Component c) {
    components.remove(c);
  }

  public List<Component> getChild() {
    return components;
  }

  public void operation() {
    StringBuilder tab = new StringBuilder("|-");
    for(int i = 0; i < level; i++)
      tab.append("-");
    System.out.println(tab + "Operation on composite (" + name + ")");
    for (Component c : components)
      c.operation();
  }

}