public class Element extends Component {
  public Element(String name) {
    super(name);
  }

  public void operation() {
    StringBuilder tab= new StringBuilder("|-");
    for(int i=0;i<level;i++)
      tab.append("-");
    System.out.println(tab + "Operation on element (" + name + ")");
  }
}