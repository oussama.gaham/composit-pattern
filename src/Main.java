public class Main {
  public static void main(String[] args) {
    Composite root = new Composite("Composite 1");
    Composite child = new Composite("Composite 2");
    root.add(child);
    root.add(new Element("Element 1"));
    root.add(new Element("Element 2"));
    root.add(new Element("Element 3"));
    child.add(new Element("Element 4"));
    child.add(new Element("Element 5"));
    root.operation();
  }
}
